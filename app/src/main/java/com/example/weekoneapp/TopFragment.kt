package com.example.weekoneapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.weekoneapp.databinding.FragmentTopBinding

class TopFragment : Fragment() {
    private var binding: FragmentTopBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTopBinding.inflate(layoutInflater)
        binding?.textView?.setOnClickListener(){
            findNavController().navigate(R.id.action_topFragment_to_imageResultFragment)
        }
        binding?.textView2?.setOnClickListener(){
            throw RuntimeException("Fatal")
        }

        val view = binding?.root
        return view
    }
}