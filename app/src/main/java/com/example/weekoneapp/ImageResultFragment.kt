package com.example.weekoneapp

import android.app.Activity
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.example.weekoneapp.databinding.FragmentImageResultBinding

class ImageResultFragment : Fragment() {
    private var binding: FragmentImageResultBinding? = null
    private var viewW:Int = 0
    private  var viewH:Int = 0
    private var url = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = FragmentImageResultBinding.inflate(inflater, container, false)

        url = "https://picsum.photos/250/?random"
        binding?.webView?.webViewClient = MyWebViewClient(requireActivity())
        binding?.webView?.loadUrl(url)
        binding?.webView?.settings?.also {
            it.useWideViewPort = true
            it.loadWithOverviewMode = true
        }

        val view = binding?.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val observer: ViewTreeObserver? = binding?.webView?.viewTreeObserver
        observer?.addOnGlobalLayoutListener {
            viewH = (binding?.webView?.height!!).toInt()
            viewW = (binding?.webView?.width!!).toInt()
        }
        if (viewH > 0){
            var url = "https://picsum.photos/${viewW}/${viewH}/?random"
            binding?.webView?.loadUrl(url)
        }

        binding?.searchImageButton?.setOnClickListener(){
            url = "https://picsum.photos/${viewW}/${viewH}/?random"
            binding?.webView?.loadUrl(url)
            binding?.searchImageButton?.text = "もう一度！"
        }

    }

    class MyWebViewClient internal constructor(private val activity: Activity) : WebViewClient() {

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            val url: String = request?.url.toString();
            view?.loadUrl(url)
            return true
        }

        override fun shouldOverrideUrlLoading(webView: WebView, url: String): Boolean {
            webView.loadUrl(url)
            return true
        }

        override fun onReceivedError(
                view: WebView,
                request: WebResourceRequest,
                error: WebResourceError
        ) {
            Toast.makeText(activity, "Got Error! $error", Toast.LENGTH_SHORT).show()
        }
    }
}